#include <Wire.h>

//Display
#include <SPI.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SH110X.h>

#define OLED_MOSI     3
#define OLED_CLK      2
#define OLED_DC       0
#define OLED_CS       1
#define OLED_RST      4

Adafruit_SH1106G display = Adafruit_SH1106G(128, 64,OLED_MOSI, OLED_CLK, OLED_DC, OLED_RST, OLED_CS);

//GNSS
#include <TinyGPSPlus.h>
//#include <SoftwareSerial.h>

#define GNSS_RX 7
#define GNSS_TX 6

TinyGPSPlus gnss;
//SoftwareSerial ss(GNSS_RX, GNSS_TX);

String ISODateTime;
String Length;

//Enviroment Sensor
#include <Adafruit_BMP280.h>
Adafruit_BMP280 bmp;

//RTC
// #include <DS3231.h>
// DS3231 RTC;
// bool century = false;
// bool h12Flag = false;
// bool pmFlag;
#include <I2C_RTC.h>
static DS3231 RTC;

void setup() {
  Serial.begin(115200);

  //Display
  display.begin(0, true);
  display.display();
  delay(10);
  display.clearDisplay();

  //GNSS
  //ss.begin(9600);
  Serial1.begin(9600, SERIAL_8N1, GNSS_RX, GNSS_TX);

  bmp.begin(0x76,0x58);
  bmp.setSampling(Adafruit_BMP280::MODE_NORMAL,     /* Operating Mode. */
                  Adafruit_BMP280::SAMPLING_X2,     /* Temp. oversampling */
                  Adafruit_BMP280::SAMPLING_X16,    /* Pressure oversampling */
                  Adafruit_BMP280::FILTER_X16,      /* Filtering. */
                  Adafruit_BMP280::STANDBY_MS_500); /* Standby time. */
  
  //RTC
  RTC.begin();
  RTC.setHourMode(CLOCK_H24);

  setRTCDate();
  setRTCTime();
}

void loop() {
  while (Serial1.available() > 0)
    if (gnss.encode(Serial1.read()));
  mainDisplay();

  if(gnss.date.isValid() && gnss.date.isUpdated()){
    setRTCDate();
  }

  if(gnss.time.isValid() && gnss.time.isUpdated()){
    setRTCTime();
  }

  // setRTCDate();
  // setRTCTime();
  Serial.println(gnss.time.second());
}

void mainDisplay(){
  display.clearDisplay();
  display.setTextSize(1);

  //Top Bar
  display.fillRect(0, 0, 128, 7, SH110X_WHITE);

  display.setTextColor(SH110X_BLACK);
  display.setCursor(0, 0);

  display.print("SAT ");
  display.print(gnss.satellites.value());

  display.setCursor(110, 0);
  if (gnss.time.isValid() /*&& gnss.time.isUpdated()*/){
    display.print("T");
  } else {
    display.print(" ");
  }

  if (gnss.date.isValid() /*&& gnss.date.isUpdated()*/){
    display.print("D");
  } else {
    display.print(" ");
  }

  if (gnss.location.isValid() /*&& gnss.location.isUpdated()*/){
    display.print("L");
  } else {
    display.print(" ");
  }

  //UTC
  display.setTextColor(SH110X_WHITE);
  display.setCursor(0, 8);
  dateTime();
  display.print(ISODateTime);

  //Local Time
  display.setCursor(0, 16);
  display.print("XXXX-XX-XX  XX:XX:XXM");

  //Location
  //Length = String(gnss.location.lat(), 6);
  display.setCursor(0, 24);
  display.print(gnss.location.lat(), 6);
  display.setCursor(66, 24);
  display.print(gnss.location.lng(), 6);

  //Other Shit
  display.setCursor(0, 32);
  //display.print(Length.length());
  display.print("ALT ");
  display.print(gnss.altitude.meters());
  display.print("M  ");

  display.print(gnss.speed.kmph());
  display.print("KPH");

  //Sensor
  display.setCursor(0, 40);
  display.print(bmp.readTemperature());
  display.write(0xF7); //°
  display.print("C");
  display.setCursor(60, 40);
  display.print(bmp.readPressure()/100);
  display.print(" hPa");

  // display.setCursor(0, 48);
  // display.print(bmp.readAltitude(1038));

  display.setCursor(0, 48);
  display.print(RTC.getYear());
  display.print(RTC.getMonth());
  display.print(RTC.getDay());

  display.print(RTC.getHours());
  display.print(RTC.getMinutes());
  display.print(RTC.getSeconds());

  display.print(" ");

  display.print(RTC.getTemp());

  display.display();
}

void setRTCDate(){
  RTC.setYear(gnss.date.year());
  RTC.setMonth(gnss.date.month());
  RTC.setDay(gnss.date.day());
}

void setRTCTime(){
  RTC.setHours(gnss.time.hour());
  RTC.setMinutes(gnss.time.minute());
  RTC.setSeconds(gnss.time.second());
}

void dateTime(){
  ISODateTime = "";
  ISODateTime += String(gnss.date.year()) + "-";
  if (gnss.date.month() < 10) ISODateTime += "0";
  ISODateTime += String(gnss.date.month()) + "-";
  if (gnss.date.day() < 10) ISODateTime += "0";
  ISODateTime += String(gnss.date.day()) + "  ";
  if (gnss.time.hour() < 10) ISODateTime += "0";
  ISODateTime += String(gnss.time.hour()) += ":";
  if (gnss.time.minute() < 10) ISODateTime += "0";
  ISODateTime += String(gnss.time.minute()) += ":";
  if (gnss.time.second() < 10) ISODateTime += "0";
  ISODateTime += String(gnss.time.second()) + "Z";
}
